package cl.bancointernacional.ti.apitask;
 

import static org.assertj.core.api.Assertions.assertThat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.client.RestTemplate;
 
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@ContextConfiguration()
@SpringBootTest
public class IntegrationTests {
 
    @Autowired
    RestTemplate restTemplate;
 
    @Test
    public void getEmployees() {
        final String uri = "http://backend-schedulator:8080/generator/schedule/tasks";
 
        String result = restTemplate.getForObject(uri, String.class);
 
        assertThat(result.isEmpty()).isEqualTo(false);
    }
}