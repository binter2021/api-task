package cl.bancointernacional.ti.apitask.model;

import java.util.ArrayList;
import java.util.List;

public class TaskWorkDays
{
    private List<TaskWorkDay> taskWorkDayList;

    public List<TaskWorkDay> getTaskWorkDayList(){
        if(taskWorkDayList == null){
            taskWorkDayList = new ArrayList<>();
        }
        return taskWorkDayList;
    }
    public void setTaskWorkDayList(List<TaskWorkDay> taskWorkDayList){
        this.taskWorkDayList = taskWorkDayList;
    }
}