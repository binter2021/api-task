package cl.bancointernacional.ti.apitask.model;
import com.fasterxml.jackson.annotation.JsonGetter;

public class Schedule {
    private String taskId;
    private String taskName;
    private Integer duration;

    public Schedule() {}

    public Schedule(String taskId, String taskName, Integer duration) {
        super();
        this.taskId = taskId;
        this.taskName = taskName;
        this.duration = duration;
    }
    @JsonGetter("task_id")
    public String getTaskId(){
        return this.taskId;
    }
    public void setTaskId(String taskId){
        this.taskId = taskId;
    }
    @JsonGetter("task_name")
    public String getTaskName(){
        return this.taskName;
    }
    public void setTaskName(String taskName){
        this.taskName = taskName;
    }
    public Integer getDuration(){
        return this.duration;
    }
    public void setDuration(Integer duration){ 
        this.duration = duration;
    }
    @Override
    public String toString() {
        return "Schedule [ taskId=" + taskId+ ",  taskName=" +taskName + ", duration=" +duration + "]";
    }
}
