package cl.bancointernacional.ti.apitask.model;

public class TaskWorkDay {
    private String[] tasks;
    private String nameTask;
    private Integer duration;
    private Integer id;

    public TaskWorkDay() {}
    
    public TaskWorkDay(String[] tasks, String nameTask, Integer duration) {
        super();
        this.tasks = tasks;
        this.nameTask = nameTask;
        this.duration = duration;
    }

    public TaskWorkDay(String[] tasks, String nameTask, Integer duration, Integer id) {
        super();
        this.tasks = tasks;
        this.nameTask = nameTask;
        this.duration = duration;
        this.id = id;
    }
    public String[] getTasks(){
        return this.tasks;
    }
    public void setTasks(String[] tasks){
        this.tasks = tasks;
    }
    public String getNameTask(){
        return this.nameTask;
    }
    public void setNameTask(String nameTask){
        this.nameTask = nameTask;
    }
    public Integer getId(){
        return this.id;
    }
    public void setId(Integer id){
        this.id = id;
    }
    public Integer getDuration(){
        return this.duration;
    }
    public void setDuration(Integer duration){ 
        this.duration = duration;
    }
    @Override
    public String toString() {
        return "TaskWorkDay [id=" +id + ", tasks=" + tasks[0] + ",  nameTask=" +nameTask + ", duration=" +duration + "]";
    }
}
