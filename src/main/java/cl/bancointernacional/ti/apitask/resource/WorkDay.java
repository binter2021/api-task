package cl.bancointernacional.ti.apitask.resource;

public class WorkDay {
    private String tasks;
    private String nameTask;
    private Integer duration;
    private Integer id;
    
    public WorkDay() {}
    
    public WorkDay(String tasks, String nameTask, Integer duration) {
        super();
        this.tasks = tasks;
        this.nameTask = nameTask;
        this.duration = duration;
    }

    public WorkDay(String tasks, String nameTask, Integer duration, Integer id) {
        super();
        this.tasks = tasks;
        this.nameTask = nameTask;
        this.duration = duration;
        this.id = id;
    }
    public String getTasks(){
        return this.tasks;
    }
    public void setTasks(String tasks){
        this.tasks = tasks;
    }
    public String getNameTask(){
        return this.nameTask;
    }
    public void setNameTask(String tasks){
        this.tasks = tasks;
    }
    public Integer getId(){
        return this.id;
    }
    public void setId(Integer id){
        this.id = id;
    }
    public Integer getDuration(){
        return this.duration;
    }
    public void setDuration(Integer duration){
        this.duration = duration;
    }
}
