package cl.bancointernacional.ti.apitask.controller;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import cl.bancointernacional.ti.apitask.dao.TaskWorkDayDAO;

import cl.bancointernacional.ti.apitask.model.TaskWorkDays;

import cl.bancointernacional.ti.apitask.dto.TaskWorkDayDTO;

@CrossOrigin(origins = "*", methods= {RequestMethod.GET})
@RestController
public class TaskController {
    private static final Logger log = LoggerFactory.getLogger(TaskController.class);

    @Autowired
    private TaskWorkDayDAO taskWorkDayDAO;

    @Autowired
    private TaskWorkDayDTO taskWorkDayDTO;
    // public TaskController() {
    //     super();
    // }
    @GetMapping(value = "/tasks")
    public TaskWorkDays getAllTasksWorkDays()
    {

        return taskWorkDayDTO.getAllTasksWorkDays();
    }
}
