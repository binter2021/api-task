package cl.bancointernacional.ti.apitask.dto;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.ResponseEntity;

import cl.bancointernacional.ti.apitask.model.TaskWorkDay;
import cl.bancointernacional.ti.apitask.model.TaskWorkDays;
import cl.bancointernacional.ti.apitask.model.Schedule;
import org.springframework.stereotype.Repository;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Comparator;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
// TODO pasar a services la logica
@Repository
public class TaskWorkDayDTO {

    @Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}
    public TaskWorkDays getAllTasksWorkDays()
    {
        // TODO pasar a variable de entorno
        final String uri = "http://backend-schedulator:8080/generator/schedule/tasks";
        final Integer MAX_HR_WORK_DAY = 8;

        TaskWorkDays TaskWorkDays = new TaskWorkDays();
       
        List<TaskWorkDay> taskWorkDayList = new ArrayList<>();
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Schedule[]> responseEntity = restTemplate.getForEntity(uri, Schedule[].class);
        Schedule[] scheduleArray = responseEntity.getBody();
        // TODO separar logica del fusionador
        //Se ordena de menr a mayor el schedule para optimizar las tareas 
        Arrays.sort(scheduleArray, Comparator.comparing(Schedule::getDuration));
        //inicializo las variables temporales
        String tmpnametask = "";
        Integer acumduration = 0;
        Integer j = 1;
        ArrayList<String> listtmptask = new ArrayList<String>();

        //empieza el fusionaador
        //al schedule ordenado, se compara la duracin actual con la duracion sgte, si esta es menor a la jornada (8 horas)
        //acumulo la jornada en duracion y junto nombres e id de terea
        //caso contario paso al list
        for (int i = 0; i < scheduleArray.length; i++) {
            
            if (i+1< scheduleArray.length && acumduration + scheduleArray[i+1].getDuration() <= MAX_HR_WORK_DAY) {
                acumduration = acumduration+scheduleArray[i].getDuration();
                listtmptask.add(scheduleArray[i].getTaskId());

                tmpnametask += scheduleArray[i].getTaskName()+" ";
            }else{
                TaskWorkDay TaskWorkDay =  new TaskWorkDay();
                TaskWorkDay.setDuration(acumduration);
                TaskWorkDay.setId(j);
                TaskWorkDay.setNameTask(tmpnametask);
              
                acumduration = 0;
                tmpnametask = "";
                j++;
                String[] myArray = new String[listtmptask.size()];
                listtmptask.toArray(myArray);
                TaskWorkDay.setTasks(myArray);
                listtmptask.clear();
                if(i+1< scheduleArray.length){
                    taskWorkDayList.add(TaskWorkDay);
                }
                
                // tmptask = Arrays.(tmptask, tmptask.length + 1);
            }
        }
        TaskWorkDays.setTaskWorkDayList(taskWorkDayList);
        return TaskWorkDays;    
    }
}
