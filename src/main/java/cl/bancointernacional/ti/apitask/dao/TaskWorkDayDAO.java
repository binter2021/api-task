package cl.bancointernacional.ti.apitask.dao;

import org.springframework.stereotype.Repository;

import cl.bancointernacional.ti.apitask.model.TaskWorkDay;
import cl.bancointernacional.ti.apitask.model.TaskWorkDays;

@Repository
public class TaskWorkDayDAO
{
    private static TaskWorkDays list = new TaskWorkDays();
    static
    {
        String[] str1 = new String[] { "35951c98-a9c9-45f6-990b-4e746957f156", "66951c98-a9c9-45f6-990b-4e746957f1433" };
        String[] str2 = new String[] { "55951c98-a9c9-45f6-990b-4e746957f111", "99951c98-a9c9-45f6-990b-4e746957f1787" };
        list.getTaskWorkDayList().add(new TaskWorkDay(str1, "Tarea 2 ",7,1));
        list.getTaskWorkDayList().add(new TaskWorkDay(str2, "Tarea 1/Tarea 3 ",8,2));
    }
    public TaskWorkDays getAllTasksWorkDays()
    {
        return list;
    }
}