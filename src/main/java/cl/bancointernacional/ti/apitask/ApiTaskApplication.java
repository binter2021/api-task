package cl.bancointernacional.ti.apitask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"cl.bancointernacional.ti.apitask"})
public class ApiTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiTaskApplication.class, args);
	}

}
